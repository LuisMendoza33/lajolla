var express = require('express');
var app = express();

app.set('view engine', 'pug');
app.use(express.static('assets'));

app.get('/', function(req, res){
	res.render('index', {title: 'LA JOLLA / Laguna en Asia / Terrenos y Departamentos', id: 'ini'});
});

app.get('/condominio', function(req, res){
	res.render('index', {title: 'LA JOLLA / Laguna en Asia / Condominio', id: 'con'});
});

app.get('/departamentos', function(req, res){
	res.render('index', {title: 'LA JOLLA / Laguna en Asia / Departamentos', id: 'dep'});

});

app.get('/terrenos', function(req, res){
	res.render('index', {title: 'LA JOLLA / Laguna en Asia / Terrenos', id: 'ter'});
});

app.get('/contacto', function(req, res){
	res.render('index', {title: 'LA JOLLA / Laguna en Asia / Contacto', id: 'cont'});
});

app.get('/politicas-privacidad', function(req, res){
	res.render('politicas', {title: 'LA JOLLA / Laguna en Asia / Políticas de Privacidad'});
});

app.use(function(req, res, next) {
	res.status(404);
	res.redirect("/");
});

app.listen(3000, function(err){
	if (err) return console.log('Hubo un error'), process.exit(1);
	console.log('Accedio al puerto 3000');
})