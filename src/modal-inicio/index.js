var yo = require('yo-yo');
var $ = require("jquery");
var validate = require("jquery-validation");
var classie = require("desandro-classie");

var translate = require('../translate').message;
var formulario = require('../formulario');
var hideModal = require('../hide-modal');

module.exports = {

	content: function (){

			var el = yo`<div id="modal">
					<div class='form-overlay'></div>
					<div id='form-container'>
						<div id='form-content'>
							<div class="messages"><div class="boxEnviando">${translate('enviando')}...</div></div>

							<div id='form-head'>
								<span class='icon fa fa-close' id='form-close' onclick=${hideModal.bind()}></span>
								
								<h4 class='pre heading_32'><span class="titForm">${translate('form.titForm')}</span><br><span class="titForm2">${translate('form.titForm2')}</span></h4>
								<p class="textForm col-sm-11 col-md-11">${translate('form.textForm')}</p>
								<div class="font_semibold col-xs-12 col-sm-12 col-md-12 divDatos">
									<div class="row">
										<span class="color_grey2 llamanos">${translate('llamanos')}: </span>
										<a href="tel:+01 602 4441">(01) 602 4441</a> - <a href="tel:+51 980 477 677">(51) 980 477 677</a>
									</div>
									
								</div>
							</div>
							${ formulario.content() }
						</div>
					</div>
				</div>`;

		return el;
	},
	showModal: function (){
		var formContainer = document.getElementById('form-container');
		var formContent = document.getElementById('form-content');
		var formSubmitted = document.getElementById('form-submitted');

		var openForm = true;

		if(navigator.platform.toUpperCase().indexOf('MAC')>=0 || 
			navigator.platform=="iPhone" || 
			navigator.platform=="iPod" || 
			navigator.platform=="iPad")
		{

			formContainer.classList.add('expand');
			formContent.classList.add('expand');
			document.body.classList.add('show-form-overlay');


		}
		else{
			document.body.classList.add('show-form-overlay');

			$('#form-container').fadeIn('fast', function(){
				formContainer.classList.add('expand');
				formContent.classList.add('expand');
			})
		}
		
	}

}