var yo = require('yo-yo');
var translate = require('../translate').message;

module.exports = yo`<div class="modal fade" id="formSuccess" tabindex="-1" role="dialog" aria-labelledby="formSuccessLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<span class='icon fa fa-check iconCheck'></span>

							<div id='form-head'>
								<h4 class='pre heading_32'><span class="successTit1">${translate('form.successTit1')}</span><br><span class="successTit2">${translate('form.successTit2')}</span></h4>
								<p class="successTit3 col-sm-11 col-md-11">${translate('form.successTit3')}</p>

								<div class="contAction">
									<a href="brochure/brochure_la_jolla.pdf" class="btn btn-send descargarBro" target="_blank">${translate('desc')}</a>
									<button type="button" class="btn btn-send boxCeleste successTit4" data-dismiss="modal">${translate('form.successTit4')}</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>`;