var page = require('page');
var empty 	= require('empty-element');
var title 	= require('title');

var header = require('../header');
var template = require('./template');
var activeMenu = require('../active-menu');
var formulario = require('../formulario');
var success = require('../success');


page('/contacto', header, function(ctx, next){
	/*Seteando el titulo*/
	title('LA JOLLA / Laguna en Asia / Contacto');

	var main = document.getElementById('main-container');
	empty(main).appendChild(template.content());

	var modalShow = document.getElementById('modal');
	if (modalShow) modalShow.remove();


	/*colocar las clases para la animacion inicial*/
	var contac = document.getElementById('contacto');
	contac.classList.add('fadeIn');
	contac.classList.add('animated');
	activeMenu();

	/*Creando Modal Success*/
	document.body.appendChild(success);


	next();

}, function(ctx, next){
	formulario.formValid();
	template.loadMap();
});