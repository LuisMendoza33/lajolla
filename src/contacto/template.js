var yo = require('yo-yo');

var translate = require('../translate').message;
var formulario = require('../formulario');

module.exports = {

	content: function() {

	var el = yo`<div id="contacto" class="content">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1">
						<div class="row">
							<p class="heading_32 h_bordered2 font_bold fadeInLeft anim">Informes y visitas previa</p>
							<p class="fadeInLeftBig anim">Envíanos un correo electrónico o llámanos para programar una visita a la obra.</p>
							<ul class="contact_info list-inline fadeInLeftBig anim">
								<li class="email color_lightgrey"><a href="mailto:informes@lajolla.com.pe">informes@lajolla.com.pe</a></li>
								<li class="phone"><span class="color_lightgrey"> | </span><span class="p_r1_0">Teléfono:</span> <a href="tel:+516024441" class="font_bold">+51 602 - 4441</a></li>
							</ul>
							${ formulario.content() }

						</div>
					</div>
					<div class="space2"></div>
					<div id="cd-google-map">
						<div id="google-container"></div>
						<div id="cd-zoom-in"></div>
						<div id="cd-zoom-out"></div>
					</div>
				</div>`;

		return el;

	},

	loadMap: function(){

		//set your google maps parameters
	    var latitude = -12.7909829,
	        longitude = -76.5849324,
	        map_zoom = 14;

	    //google map custom marker icon - .png fallback for IE11
	    var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
	    var marker_url = ( is_internetExplorer11 ) ? 'images/cd-icon-location.png' : 'images/cd-icon-location.svg';

	    //define the basic color of your map, plus a value for saturation and brightness
	    var	main_color = '#000',
	        saturation_value = -100,
	        brightness_value = -5;

	    //we define here the style of the map
	    var style= [
	        {
	            //set saturation for the labels on the map
	            elementType: "labels",
	            stylers: [
	                {saturation: saturation_value}
	            ]
	        },
	        {	//poi stands for point of interest - don't show these lables on the map
	            featureType: "poi",
	            elementType: "labels",
	            stylers: [
	                {visibility: "off"}
	            ]
	        },
	        {
	            //don't show highways lables on the map
	            featureType: 'road.highway',
	            elementType: 'labels',
	            stylers: [
	                {visibility: "off"}
	            ]
	        },
	        {
	            //don't show local road lables on the map
	            featureType: "road.local",
	            elementType: "labels.icon",
	            stylers: [
	                {visibility: "off"}
	            ]
	        },
	        {
	            //don't show arterial road lables on the map
	            featureType: "road.arterial",
	            elementType: "labels.icon",
	            stylers: [
	                {visibility: "off"}
	            ]
	        },
	        {
	            //don't show road lables on the map
	            featureType: "road",
	            elementType: "geometry.stroke",
	            stylers: [
	                {visibility: "off"}
	            ]
	        },
	        //style different elements on the map
	        {
	            featureType: "transit",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "poi",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "poi.government",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "poi.sport_complex",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "poi.attraction",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "poi.business",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "transit",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "transit.station",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "landscape",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]

	        },
	        {
	            featureType: "road",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "road.highway",
	            elementType: "geometry.fill",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        },
	        {
	            featureType: "water",
	            elementType: "geometry",
	            stylers: [
	                { hue: main_color },
	                { visibility: "on" },
	                { lightness: brightness_value },
	                { saturation: saturation_value }
	            ]
	        }
	    ];

	    //set google map options
	    var map_options = {
	        center: new google.maps.LatLng(latitude, longitude),
	        zoom: map_zoom,
	        panControl: false,
	        zoomControl: false,
	        mapTypeControl: false,
	        streetViewControl: false,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        scrollwheel: false,
	        styles: style
	    };
	    //inizialize the map
	    var map = new google.maps.Map(document.getElementById('google-container'), map_options);
	    //add a custom marker to the map
	    var marker = new google.maps.Marker({
	        position: new google.maps.LatLng(latitude, longitude),
	        map: map,
	        visible: true,
	        icon: marker_url
	    });

	    //add custom buttons for the zoom-in/zoom-out on the map
	    function CustomZoomControl(controlDiv, map) {
	        //grap the zoom elements from the DOM and insert them in the map
	        var controlUIzoomIn= document.getElementById('cd-zoom-in'),
	            controlUIzoomOut= document.getElementById('cd-zoom-out');
	        controlDiv.appendChild(controlUIzoomIn);
	        controlDiv.appendChild(controlUIzoomOut);

	        // Setup the click event listeners and zoom-in or out according to the clicked element
	        google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
	            map.setZoom(map.getZoom()+1)
	        });
	        google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
	            map.setZoom(map.getZoom()-1)
	        });
	    }

	    var zoomControlDiv = document.createElement('div');
	    var zoomControl = new CustomZoomControl(zoomControlDiv, map);

	    //insert the zoom div on the top left of the map
	    map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
	}
	

}
