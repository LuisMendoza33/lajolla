module.exports = function(){

	var formContainer = document.getElementById('form-container');
	var formContent = document.getElementById('form-content');
	var formSubmitted = document.getElementById('form-submitted');

	var openForm = false;


	if(navigator.platform.toUpperCase().indexOf('MAC')>=0) {

		document.body.classList.remove('show-form-overlay');	
		formContainer.classList.remove('expand');
		formContent.classList.remove('expand');

	}
	else{
		formContainer.classList.remove('expand');
		formContent.classList.remove('expand');
		document.body.classList.remove('show-form-overlay');

		$('#form-container').fadeOut('slow');
	}
}