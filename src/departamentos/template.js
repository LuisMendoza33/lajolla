var yo = require('yo-yo');

module.exports = function(){

	var el = yo`<div id="depas" class="content">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<div class="row">
						<div class="col-md-6 p_l_0">
							<p class="heading_32 h_bordered2 font_bold fadeInLeft anim">Luxury Departments</p>
							<p class="fadeInLeftBig anim">Edificio de 10 pisos ubicado en la cuarta fila, con departamentos desde 250m2 y 500m2, de 3,4 o 5 dormitorios y la mejor vista a la laguna & océano.</p>
						</div>
						<div class="space2"></div>
						<div class="lightbox-gallery fadeInUpBig anim">
							<div class="portfolio-sizer"></div>
							<div class="portfolio-item portfolio-item--width2 portfolio-item--height2">
								<a href="images/departamentos/img1.jpg">
									<div class="project-image"><img src="/images/departamentos/img1.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item">
								<a href="images/departamentos/img2.jpg">
									<div class="project-image"><img src="/images/departamentos/img2.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item">
								<a href="images/departamentos/img3.jpg">
									<div class="project-image"><img src="/images/departamentos/img3.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item portfolio-item--width2">
								<a href="/images/departamentos/img4.jpg">
									<div class="project-image"><img src="/images/departamentos/img4.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item">
								<a href="/images/departamentos/img5.jpg">
									<div class="project-image"><img src="/images/departamentos/img5.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item">
								<a href="/images/departamentos/img6.jpg">
									<div class="project-image"><img src="/images/departamentos/img6.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item">
								<a href="/images/departamentos/img7.jpg">
									<div class="project-image"><img src="/images/departamentos/img7.jpg" alt="image"/></div>
								</a>
							</div>
							<div class="portfolio-item">
								<a href="/images/departamentos/img8.jpg">
									<div class="project-image"><img src="/images/departamentos/img8.jpg" alt="image"/></div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>`;

	return el;

};
