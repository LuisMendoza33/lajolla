var page = require('page');
var empty 	= require('empty-element');
var title 	= require('title');

var header = require('../header');
var template = require('./template');

var lightbox = require('../lightbox');
var activeMenu = require('../active-menu');

page('/departamentos', header, function(ctx, next){
	/*Seteando el titulo*/
	title('LA JOLLA / Laguna en Asia / Departamentos');

	var main = document.getElementById('main-container');
	empty(main).appendChild(template());	

	var modalShow = document.getElementById('modal');
	if (modalShow) modalShow.remove();

	var MapGoogle = document.getElementById('MapGoogle');
	if (MapGoogle) MapGoogle.remove();

	/*colocar las clases para la animacion inicial*/
	var depas = document.getElementById('depas');
	depas.classList.add('fadeIn');
	depas.classList.add('animated');
	activeMenu();

	lightbox();

	/*Remover el Modal Success*/
	var formSuccessModal = document.getElementById("formSuccess");
	if (formSuccessModal) formSuccessModal.remove();

});