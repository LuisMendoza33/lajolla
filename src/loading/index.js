var yo = require('yo-yo');
var empty = require('empty-element');

var el = yo`<div id="loader-bg" class="loader-bg">
		<div class="loader"></div>
	</div>`;

module.exports = function loading(ctx){

	var loader = document.createElement('section');
	loader.setAttribute("id","loading");
	//var loader = document.getElementById('loading');

	document.body.appendChild(loader);
	empty(loader).appendChild(el);

	setTimeout(function() {
		loader.classList.add('fadeOut');
		loader.classList.add('animated');

		setTimeout(function() {
			loader.remove();
		}, 1500);
	}, 2000);
}