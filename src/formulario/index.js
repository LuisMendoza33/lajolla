var yo = require('yo-yo');
var $ = require("jquery");
var validate = require("jquery-validation");
var classie = require("desandro-classie");
var translate = require('../translate').message;
var formSuccess = require('../success');
var hideModal = require('../hide-modal');


module.exports = {
	content: function() {
		
		var el = yo`<div>
					<div class="messages"><div class="boxEnviando">${translate('enviando')}...</div></div>
					<form id="contact-form" class="fadeInUpBig anim">
						<input type="hidden" id="action" name="action" value="contacto">
						<div class="controls">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="nombre" name="nombre" class="input__field input__field--kohana form-control" type="text" />
										<label class="input__label input__label--kohana" for="nombre">
											<i class="fa fa-user icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formNom">${translate('form.formNom')}</span>
										</label>
									</span>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="apellido" name="apellido" class="input__field input__field--kohana form-control" type="text" />
										<label class="input__label input__label--kohana" for="apellido">
											<i class="fa fa-user icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formApe">${translate('form.formApe')}</span>
										</label>
									</span>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="correo" name="correo" class="input__field input__field--kohana form-control" type="email" />
										<label class="input__label input__label--kohana" for="correo">
											<i class="fa fa-envelope icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formCor">${translate('form.formCor')}</span>
										</label>
									</span>
								</div>
								<div class="col-xs-5 col-sm-5 col-md-5 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="dni" name="dni" class="input__field input__field--kohana form-control" type="text" />
										<label class="input__label input__label--kohana" for="dni">
											<i class="fa fa-id-card icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formDni">${translate('form.formDni')}</span>
										</label>
									</span>
								</div>
								<div class="col-xs-5 col-sm-5 col-md-5 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="telefono" name="telefono" class="input__field input__field--kohana form-control" type="text" />
										<label class="input__label input__label--kohana" for="telefono">
											<i class="fa fa-phone icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formTel">${translate('form.formTel')}</span>
										</label>
									</span>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 inputMarginTop">
									<div class="form-group">
										<select id="interesado" name="interesado" class="form-control">
											<option value="" selected class="formInt">${translate('form.formInt')}</option>
											<option value="0" class="formInt1">${translate('form.formInt1')}</option>
											<option value="1" class="formInt2">${translate('form.formInt2')}</option>
										</select>
									</div>
								</div>
								<div class="col-xs-7 col-sm-7 col-md-7 inputMarginTop">
									<div class="form-group">
										<select id="actualmente" name="actualmente" class="form-control">
											<option value="" selected class="formEnc">${translate('form.formEnc')}</option>
											<option value="Facebook">${translate('form.formEnc1')}</option>
											<option value="Revista Cosas">${translate('form.formEnc2')}</option>
											<option value="Revista Casas">${translate('form.formEnc3')}</option>
											<option value="Revista H">${translate('form.formEnc4')}</option>
											<option value="Revista Prestigia">${translate('form.formEnc5')}</option>
											<option value="Revista La Guía Inmobiliaria">${translate('form.formEnc6')}</option>
											<option value="Encarte Casas de Playa">${translate('form.formEnc7')}</option>
											<option value="Paneles Panamericana Sur">${translate('form.formEnc8')}</option>
											<option value="Paneles LED Surco">P${translate('form.formEnc9')}</option>
											<option value="Stand Boulevard Asia">${translate('form.formEnc10')}</option>
											<option value="Amigos">${translate('form.formEnc11')}</option>
											<option value="Otros" class="formEnc12">${translate('form.formEnc12')}</option>
										</select>
									</div>
								</div>
								<div class="col-xs-5 col-sm-5 col-md-5 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="distrito" name="distrito" class="input__field input__field--kohana form-control" type="text" />
										<label class="input__label input__label--kohana" for="distrito">
											<i class="fa fa-map-marker icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formDis">${translate('form.formDis')}</span>
										</label>
									</span>
								</div>
								<div class="col-xs-5 col-sm-6 col-md-6 inputMarginTop">
									<div class="form-group">
										<select id="ninos" name="ninos" class="form-control">
											<option value="" selected class="formHij">${translate('form.formHij')}</option>
											<option value="1" class="formHij1">${translate('form.formHij1')}</option>
											<option value="0" class="formHij2">${translate('form.formHij2')}</option>
										</select>
									</div>
								</div>
								<div class="col-xs-7 col-sm-6 col-md-6 inputMarginTop">
									<span class="form-group input input--kohana">
										<input id="estudian" name="estudian" class="input__field input__field--kohana form-control" type="text" />
										<label class="input__label input__label--kohana" for="estudian">
											<i class="fa fa-bandcamp icon icon--kohana"></i>
											<span class="input__label-content input__label-content--kohana formEst">${translate('form.formEst')}</span>
										</label>
									</span>
								</div>
							</div>
							<div class="row rowMarginTop terminosBox">
								<div class="col-sm-7 col-md-7">
									<div class="checkbox">
										<input type="checkbox" id="terminos" name="terminos" class="" value="terminos">
										<label for="terminos" class="check">
											<span class="formTer1">${translate('form.formTer1')}</span>
											<a href="/politicas-privacidad" target="_blank" class="formTer2">${translate('form.formTer2')}</a>
										</label>
									</div>								
								</div>

								<div class="col-sm-5 col-md-5">
									<input type="submit" class="btn btn-send formBot" value="${translate('form.formBot')}" >
								</div>
							</div>
						</div>
					</form>
				</div>`;

		return el;
	},
	formValid: function(){

		$('#contact-form').validate({
			rules: {
				nombre: "required",
				apellido: "required",
				correo: {required: true, email: true },
				dni: {required: true, minlength: 8},
				telefono: {required: true, minlength: 7},
				interesado: "required",
				actualmente: "required",
				distrito: "required",
				ninos: "required",
				terminos: "required"
			},
			messages: { 
				nombre: "*",
				apellido: "*",
				correo: "*",
		    	dni: "*",
				telefono: "*",
				interesado: "*",
				actualmente: "*",
				distrito: "*",
				ninos: "*",
				terminos: "*"
			},
			submitHandler: function() {
				var form = $('#contact-form');

				$.ajax({
					type: "POST",
					//url: "http://www.collective.pe/clientes/lajolla/action_remoto.php",
					data: form.serialize(),
					success: function(data){

						form[0].reset();

						$("input.formBot").prop('disabled', false);
						$("input.formBot").css({'opacity':1});

						/*Si estamos en el home*/
						if (document.getElementById("home")) {
							hideModal();
						};

						setTimeout(function() {
							jQuery('#formSuccess').modal({
								backdrop: 'static',
								keyboard: false,
								show: true
							});
						},500);

						$(".messages").hide();
						

					},
					beforeSend:function(){
						$(".messages").fadeIn('slow');

						$("input.formBot").prop('disabled', true);
						$("input.formBot").css({'opacity':0});
						
					}

				});
			}

		});

		
		function inputStyle() {
			if (!String.prototype.trim) {
				(function() {
					var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
					String.prototype.trim = function() {
						return this.replace(rtrim, '');
					};
				})();
			}

			[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
				// in case the input is already filled..
				if( inputEl.value.trim() !== '' ) {
					classie.add( inputEl.parentNode, 'input--filled' );
				}

				// events:
				inputEl.addEventListener( 'focus', onInputFocus );
				inputEl.addEventListener( 'blur', onInputBlur );
			} );

			function onInputFocus( ev ) {
				classie.add( ev.target.parentNode, 'input--filled' );
			}

			function onInputBlur( ev ) {
				if( ev.target.value.trim() === '' ) {
					classie.remove( ev.target.parentNode, 'input--filled' );
				}
			}
		};

		return inputStyle();

	}
}
 