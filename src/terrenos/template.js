var yo = require('yo-yo');

module.exports = function(){

	var el = yo`<div id="terrenos" class="content">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1">
						<div class="row">
							<div class="col-md-6 p_l_0">
								<p class="heading_32 h_bordered2 font_bold fadeInLeft anim">Últimos Terrenos disponibles:</p>
								<p class="fadeInLeftBig anim">Tenemos en segunda y tercera fila de 300m2 con capacidad para construir hasta 2 pisos y una terraza en el tercer piso y con sus propios estacionamientos.</p>
							</div>
							<div class="space"></div>
							<div class="lightbox-gallery fadeInUpBig anim">
								<div class="portfolio-sizer"></div>
								<div class="portfolio-item portfolio-item--width2 portfolio-item--height2">
									<a href="images/terrenos/img1.jpg">
										<div class="project-image"><img src="/images/terrenos/img1.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="images/terrenos/img2.jpg">
										<div class="project-image"><img src="/images/terrenos/img2.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="images/terrenos/img3.jpg">
										<div class="project-image"><img src="/images/terrenos/img3.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item portfolio-item--width2">
									<a href="/images/terrenos/img4.jpg">
										<div class="project-image"><img src="/images/terrenos/img4.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="/images/terrenos/img5.jpg">
										<div class="project-image"><img src="/images/terrenos/img5.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="/images/terrenos/img6.jpg">
										<div class="project-image"><img src="/images/terrenos/img6.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="/images/terrenos/img7.jpg">
										<div class="project-image"><img src="/images/terrenos/img7.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="/images/terrenos/img8.jpg">
										<div class="project-image"><img src="/images/terrenos/img8.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="/images/terrenos/img9.jpg">
										<div class="project-image"><img src="/images/terrenos/img9.jpg" alt="image"/></div>
									</a>
								</div>
								<div class="portfolio-item">
									<a href="/images/terrenos/img10.jpg">
										<div class="project-image"><img src="/images/terrenos/img10.jpg" alt="image"/></div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>`;

	return el;

}
