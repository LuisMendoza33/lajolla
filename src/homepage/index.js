var page = require('page');
var empty 	= require('empty-element');
var vegas 	= require('vegas');
var title 	= require('title');
var validate = require("jquery-validation");

var header = require('../header');
var modal = require('../modal-inicio');
var formulario = require('../formulario');
var success = require('../success');

var template = require('./template');


page('/', header, function(ctx, next){
	/*Seteando el titulo*/
	title('LA JOLLA / Laguna en Asia / Terrenos y Departamentos');

	var main = document.getElementById('main-container');
	empty(main).appendChild(template(ctx.videoInicio));

	

	/*Plugins que corresponden al Inicio*/
	/*Vegas*/
	$(".vegas").vegas({
		timer:false,
		delay: 8000,
		slides: [
			{src: "/images/slider/01.jpg"},
			{src: "/images/slider/02.jpg"},
			{src: "/images/slider/03.jpg"}
		]
	});

	/*Formulario Script Validacion*/
	formulario.formValid();

	/*colocar las clases para la animacion inicial*/
	var home = document.getElementById('home');
	home.classList.add('fadeIn');
	home.classList.add('animated');

	var MapGoogle = document.getElementById('MapGoogle');
	if (MapGoogle) MapGoogle.remove();

	/*Creando Modal Success*/
	document.body.appendChild(success);

});