var yo = require('yo-yo');
var YouTubePlayer = require('youtube-player');

var translate = require('../translate').message;
var modal = require('../modal-inicio');

module.exports = function videoInicio(){

	var el = yo`<div id="home" class="d_table content">
					<div class="vegas"></div>

					<div class="v_table textBottom">
						<h3 class="font_bold heading_21 color_white"><span class="textSlider">${translate('textSlider')}</span> <span class="fontPlayFairDisplay textSlider2">${translate('textSlider2')}</span></h3>
					</div>

					<button id="launchForm" class="launchForm" onclick=${modal.showModal.bind()}><i class="fa fa-envelope-o"></i></button>

					<button id="launchVideo" class="launchVideo animated" onclick=${video.bind()}><i class="fa fa-play"></i></button>
					<button id="closeVideo" class="closeVideo animated hideV" onclick=${videoClose.bind()}><i class="fa fa-times"></i></button>

					<div id="VideoJolla" class="VideoJolla animated hideV">
						<div id="videoBack"></div>
					</div>

				</div>`;

	var player,
		stateNames;

	function toggleButtons(){
		document.getElementById('launchVideo').classList.toggle('hideV');
		document.getElementById('closeVideo').classList.toggle('hideV');
		document.getElementById('VideoJolla').classList.toggle('hideV');
		document.getElementById('launchForm').classList.toggle('hideV');
	}

	function video() {
		toggleButtons();
		$(".vegas").vegas('pause');	

		setTimeout(function(){
			player.playVideo();
		},200);
	}

	function videoClose(){
		toggleButtons();
		$(".vegas").vegas('play');

		player.pauseVideo();
	}

	function reproVideo(){
		stateNames = {
			'-1': 'unstarted',
			0: 'ended',
			1: 'playing',
			2: 'paused',
			3: 'buffering',
			5: 'video cued'
		};

		player = YouTubePlayer('videoBack', {
			videoId: 'tbValLoJdT8',
			playerVars: {
				'autoplay': 0,
				'controls': 0, 
				'modestbranding': 1,
				'autohide': 0,
				'showinfo' : 0,
				'color': 'red',
				'rel': 0,
				'loop': 0,
				'disablekb': 0,
				'enablejsapi': 1,
				'fs': 0,
				'hl': null,
				'iv_load_policy': 3,
				'playsinline': 0,
				'start': 0,
				'end': 0,
				'quality': 'hd720'
			}
		});

		player.on('ready', function (event) {
			console.log('El video esta listo.');
		});
		
		player.on('stateChange', function (event) {
			if (!stateNames[event.data]) {
				throw new Error('Unknown state (' + event.data + ').');
			}

			if (stateNames[event.data] == 'ended') {
				videoClose();
				player.stopVideo()
			};
		});

	}
	
	setTimeout(function(){
		reproVideo();
	},1000);
	
	

	document.body.appendChild(modal.content());
	modal.showModal();
	
	return el;
}
