var yo = require('yo-yo');
var empty = require('empty-element');
var $ = require("jquery");

var translate = require('../translate').message;

var el = yo`<div>
				<div class="brand">
					<a href="/"><span>LA JOLLA / Laguna en Asia / Terrenos y Departamentos</span></a>
				</div>
				<ul id="lang_list" class="list-inline social_list pull-right lang_list">
					<li><a id="esBtn" href="#!" onclick=${lang.bind(null, 'es')} class="active">ES</a></li>
					<li><a id="enBtn" href="#!" onclick=${lang.bind(null, 'en-US')}>EN</a></li>
				</ul>
				<ul class="list-inline social_list pull-right">
					<li><a href="https://www.facebook.com/LaJollaAsiaPeru/" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					<li><a href="mailto:informes@lajolla.com.pe"><i class="fa fa-envelope-o"></i></a></li>
					<li><a href="tel:+51 980 477 677"><i class="fa fa-whatsapp"></i></a></li>
				</ul>
				<nav class="d_table">

					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="nav">
						<ul id="navigation" class="list-inline v_table">
							<li class="color_grey2 menuInin ${window.location.pathname == '/' ? 'active' : ''}">
								<a href="/">${translate('menu.menuInin')}</a>
							</li>
							<li class="color_grey2 menuCond ${window.location.pathname == '/condominio' ? 'active' : ''}">
								<a href="/condominio">${translate('menu.menuCond')}</a>
							</li>
							<li class="color_grey2 menuDepa ${window.location.pathname == '/departamentos' ? 'active' : ''}">
								<a href="/departamentos">${translate('menu.menuDepa')}</a>
							</li>
							<li class="color_grey2 menuTerr ${window.location.pathname == '/terrenos' ? 'active' : ''}">
								<a href="/terrenos">${translate('menu.menuTerr')}</a>
							</li>
							<li class="color_grey2 menuCont ${window.location.pathname == '/contacto' ? 'active' : ''}">
								<a href="/contacto">${translate('menu.menuCont')}</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>`;

function lang(locale, ev) {

	var acLangBtn = ev.target;
	var esBtn = document.getElementById('esBtn');
	var enBtn = document.getElementById('enBtn');

	localStorage.setItem("Lang", ev.target.id);

	esBtn.classList.remove("active");
	enBtn.classList.remove("active");
	acLangBtn.classList.add('active');
	
	localStorage.locale = locale;

	location.reload();

	return false;
}




module.exports = function(ctx, next) {
	var headerCont = document.getElementById('header');
	headerCont.appendChild(el);

	var lang = localStorage.getItem("Lang");
	var esBtn = document.getElementById('esBtn');
	var enBtn = document.getElementById('enBtn');

	if (lang != null) {
		esBtn.classList.remove("active");
		enBtn.classList.remove("active");

		var setStyleLang = document.getElementById(lang);
		setStyleLang.classList.add('active');
	}

	/*Navigation*/
	var $window = $(window);
	var $navLink = $('#nav > ul > li a');
	var $brandLink = $('.brand a');

	$navLink.on('click', function (event) {
		$navLink.parent('li').removeClass('active');
		$(this).parent('li').addClass('active');
		$window.scrollTop(0);

		/* Cerrar Navigation cuando se hace click en movil */
		$opened = $("#nav").hasClass("collapse in");
		if ($opened === true) {
			$("#nav").removeClass('in').attr('aria-expanded',false);
		}
	});

	$brandLink.on('click', function () {
		$navLink.parent('li').removeClass('active');

		$('.menuInin').addClass('active');
		$window.scrollTop(0);
	});


	/*Navigation*/

	next();

}

