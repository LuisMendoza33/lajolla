var page = require('page');
var empty 	= require('empty-element');
var title 	= require('title');
var Swiper 	= require('swiper');

var header = require('../header');
var template = require('./template');

var lightbox = require('../lightbox');
var activeMenu = require('../active-menu');

page('/condominio', header, function(ctx, next){
	/*Seteando el titulo*/
	title('LA JOLLA / Laguna en Asia / Condominio');

	var main = document.getElementById('main-container');
	empty(main).appendChild(template());

	var modalShow = document.getElementById('modal');
	if (modalShow) modalShow.remove();

	var MapGoogle = document.getElementById('MapGoogle');
	if (MapGoogle) MapGoogle.remove();

	/*colocar las clases para la animacion inicial*/
	var cond = document.getElementById('condominio');
	cond.classList.add('fadeIn');
	cond.classList.add('animated');
	activeMenu();

	lightbox();

	var swiper = new Swiper('.swiper', {
		pagination: '.swiper-pagination',
		slidesPerView: 3,
		paginationClickable: true,
		spaceBetween: 40,
		grabCursor: true,
		breakpoints: {
			1024: {
				slidesPerView: 3
			},
			768: {
				slidesPerView: 2
			},
			480: {
				slidesPerView: 1
			}
		}
	});

	/*Remover el Modal Success*/
	var formSuccessModal = document.getElementById("formSuccess");
	if (formSuccessModal) formSuccessModal.remove();

});