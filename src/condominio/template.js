var yo = require('yo-yo');

var el = yo`<div id="condominio" class="content">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1">
			<div class="row">
				<div class="col-md-6 p_l_0">
					<p class="heading_32 h_bordered2 font_bold fadeInLeft anim">Inigualable y exclusiva laguna en Asia con servicios de primera. La Jolla es un condominio sofisticado y de lujo en todos sus detalles.</p>

					<p class="fadeInLeftBig anim">Con un diseño del Arq. Bernardo Fort Brescia (Arquitectónica), el primero que este realiza en la playa de Asia, este proyecto brinda la sofisticación y lujo al que están acostumbradas las personas más exigentes del mundo.</p>
					<div class="spaceText"></div>
					<p class="fadeInLeftBig anim">La compra de las unidades inmobiliarias (terrenos y departamentos) está sujeta a la aprobación de una Junta Calificadora.</p>

				</div>
				<div class="col-md-6 p_r_0">
					<div class="space2"></div>
					<img src="images/condominio_img1.jpg" class="img-responsive img-rounded img_shadow fadeInUpBig anim" alt="">
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 p_l_0">
					<div class="space2 visible-xs visible-sm"></div>
					<img src="images/condominio_img2.jpg" class="img-responsive img-rounded img_shadow fadeInUp anim" alt="">
				</div>
				<div class="col-xs-12 col-md-6 p_r_0 pull-right">
					<div class="row">
						<div class="space2"></div>
						<p class="fadeInLeftBig anim">Los accesos al proyecto desde la playa y desde el exterior están debidamente ubicados, señalizados y brindan toda la seguridad que sus residentes exigen. El proyecto se entregará con sus pistas debidamente acabadas, malecones, con pisos decorativos y una ciclovía de más de 2.5 kms, todas debidamente señalizadas e iluminadas.</p>
						<div class="spaceText"></div>
						<p class="fadeInLeftBig anim">La Jolla cuenta con un restaurante-bar, cafetería y gimnasio-spa, todos al lado de la laguna y del mar. También cuenta con canchas de tenis, fulbito y juegos para niños.</p>
					</div>
				</div>
            </div>
        </div>
		<div class="space2"></div>
		<div class="plot-plan">
			<div class="col-md-12">
				<div class="row">
					<img src="images/plotplan.jpg" class="img-responsive fadeInUp anim" alt="">
				</div>
			</div>
		</div>

		<div class="areas-comunes">
			<div class="space2"></div>
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<div class="row">
					<p class="heading_32 font_bold h_bordered22 color_white">Áreas Comunes</p>
				</div>
				<div class="space"></div>
				<div class="row">
					<div class="swiper-container swiper">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Restaurantes</p>
										<p>Restaurante Fusión de moderno diseño.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Gimnasio – Spa</p>
										<p>Máquinas elípticas, cama de pilates, sauna, masajes, camerinos, duchas.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Kids Club</p>
										<p>Cafetería, heladería, chocolatería y minicine para toda la familia.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Laguna de Agua Cristalina</p>
										<p>Espejo de agua de 55,000 m2, diseñada por Crystal LagoonsLLC.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Portada de ingreso</p>
										<p>Cuenta con un control de seguridad con video y control de acceso automatico.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Fuente de Entrada</p>
										<p>Fuente de agua de diseño profesional.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Malecón</p>
										<p>Importante malecón de 1.6km ubicado a 3 metros de altura sobre la playa.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Paisajismo Profesional</p>
										<p>Más de 130,000 m2 en áreas de paisajismo profesional.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Marinas</p>
										<p>6 Marinas dentro de la laguna para embarcaciones menores.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Jardines Colgantes</p>
										<p>Jardines colgantes al borde del malecon frente al mar.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Ciclo vía</p>
										<p>De aproximadamente 3km alrededor del proyecto.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Piscinas</p>
										<p>3 Piscinas de agua dulce en zona de niños, restaurante y gimnasio.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Estacionamientos</p>
										<p>470 estacionamientos para visitantes del proyecto.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Acceso Directo</p>
										<p>Pistas de accesos vehiculares llegan a cada residencia.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Playa Pública</p>
										<p>Con frente de 1 km de largo y acceso con control de ingreso.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Playas Privadas</p>
										<p>De arena blanca alrededor de toda la laguna.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Áreas de Servicio</p>
										<p>Minimarket.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Cancha de Tenis</p>
										<p>9 canchas de tenis de arcilla a lo largo del proyecto.</p>
									</div>
								</div>
							</div>
							<div class="swiper-slide">
								<div class="item">
									<div class="item_img">
										<img src="images/img.jpg" class="img-responsive" alt="">
									</div>
									<div class="item_content">
										<p class="heading_16 font_bold">Juego para Niños</p>
										<p>Zonas de juegos para niños.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
			<div class="space2"></div>
		</div>

		<div id="galeria">
			<div class="space2"></div>
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<div class="row">
					<div class="col-md-6 p_l_0">
						<p class="heading_32 h_bordered2 font_bold fadeInLeft anim">Galería</p>
					</div>
					<div class="space2"></div>
					<div class="lightbox-gallery fadeInUpBig anim">
						<div class="portfolio-sizer"></div>
						<div class="portfolio-item">
							<a href="images/condominio/img1.jpg">
								<div class="project-image"><img src="/images/condominio/img1.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="images/condominio/img2.jpg">
								<div class="project-image"><img src="/images/condominio/img2.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="images/condominio/img3.jpg">
								<div class="project-image"><img src="/images/condominio/img3.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img4.jpg">
								<div class="project-image"><img src="/images/condominio/img4.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img5.jpg">
								<div class="project-image"><img src="/images/condominio/img5.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img6.jpg">
								<div class="project-image"><img src="/images/condominio/img6.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img7.jpg">
								<div class="project-image"><img src="/images/condominio/img7.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img8.jpg">
								<div class="project-image"><img src="/images/condominio/img8.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img9.jpg">
								<div class="project-image"><img src="/images/condominio/img9.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img10.jpg">
								<div class="project-image"><img src="/images/condominio/img10.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img11.jpg">
								<div class="project-image"><img src="/images/condominio/img11.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img12.jpg">
								<div class="project-image"><img src="/images/condominio/img12.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img13.jpg">
								<div class="project-image"><img src="/images/condominio/img13.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img14.jpg">
								<div class="project-image"><img src="/images/condominio/img14.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img15.jpg">
								<div class="project-image"><img src="/images/condominio/img15.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img16.jpg">
								<div class="project-image"><img src="/images/condominio/img16.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img17.jpg">
								<div class="project-image"><img src="/images/condominio/img17.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img18.jpg">
								<div class="project-image"><img src="/images/condominio/img18.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img19.jpg">
								<div class="project-image"><img src="/images/condominio/img19.jpg" alt="image"/></div>
							</a>
						</div>
						<div class="portfolio-item">
							<a href="/images/condominio/img20.jpg">
								<div class="project-image"><img src="/images/condominio/img20.jpg" alt="image"/></div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>`;

module.exports = function(){
	return el;	
};
