var yo = require('yo-yo');
var translate = require('../translate').message;

var el = yo`<footer class="background_white">
  <div class="h_contact">
    <div class="font_semibold"><span class="color_grey2 llamanos">${translate('llamanos')}: </span><a href="tel:+01 602 4441">(01) 602 4441</a> - <a href="tel:+51 980 477 677">(51) 980 477 677</a></div>
    <div class="font_semibold"><span class="color_grey2 escribenos">${translate('escribenos')}: </span><a href="mailto:informes@lajolla.com.pe">informes@lajolla.com.pe</a></div>
  </div>
  <div class="copyright color_grey2 font_semibold">Km. 101 Panamericana Sur - Asia  <span class="font_bold">LUXURY Life</span></div>
</footer>`;

document.body.appendChild(el)