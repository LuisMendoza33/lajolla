var $ = require("jquery");
var magnificPopup = require('magnific-popup');


module.exports = function(){

	$('.lightbox-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-with-zoom',
		fixedContentPos: true,
		closeBtnInside: true,
		zoom: {
			enabled: true,

			duration: 300,
			easing: 'ease-in-out',
			opener: function (openerElement) {
				return openerElement.is('img') ? openerElement : openerElement.find('img');
			}
		},
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0, 1]
		}
	});
	
}