var gulp 				= require('gulp'),
	stylus 				= require('gulp-stylus'),
	plumber 			= require('gulp-plumber'),
	nib 				= require('nib'),
	livereload 			= require('gulp-livereload'),
	imagemin 			= require('gulp-imagemin'),
	imageminPngcrush 	= require('imagemin-pngcrush'),
	rename				= require('gulp-rename'),
	babel				= require('babelify'),
	browserify			= require('browserify'),
	source				= require('vinyl-source-stream'),
	concat 				= require('gulp-concat'),
	minifyCss 			= require('gulp-minify-css');

gulp.task('complementCss', function(){
	var cssFiles = ['styles/plugins/*.css'];

	gulp.src(cssFiles)
	.pipe(plumber())
	.pipe(concat('complements.css'))
	.pipe(minifyCss({compatibility: 'ie8'}))
	.pipe(gulp.dest('assets/css'));
});


gulp.task('styles', function(){
	gulp.src('styles/style.styl')
	.pipe(plumber())
	.pipe(stylus({ compress: true, use: nib(), import: ['nib'], 'include css': true, include: ['./node_modules/../']}))
	.pipe(gulp.dest('assets/css'))
	.pipe(livereload());
});

/*comprimir imagenes*/
gulp.task('imagesGen', function(){  
	gulp
		.src('images/general/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin({ plugins: [imageminPngcrush()] }))
		.pipe(gulp.dest('assets/images'))
});
gulp.task('imagesSlider', function(){  
	gulp
		.src('images/slider/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin({ plugins: [imageminPngcrush()] }))
		.pipe(gulp.dest('assets/images/slider'))
});

gulp.task('imagesDepas', function(){  
	gulp
		.src('images/departamentos/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin({ plugins: [imageminPngcrush()] }))
		.pipe(gulp.dest('assets/images/departamentos'))
});

gulp.task('imagesTerre', function(){  
	gulp
		.src('images/terrenos/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin({ plugins: [imageminPngcrush()] }))
		.pipe(gulp.dest('assets/images/terrenos'))
});

gulp.task('imagesCond', function(){  
	gulp
		.src('images/condominio/*.{png,jpg,jpeg,gif,svg}')
		.pipe(imagemin({ plugins: [imageminPngcrush()] }))
		.pipe(gulp.dest('assets/images/condominio'))
});

/**/
gulp.task('scripts', function(){
	browserify('./src/index.js')
		.transform(babel)
		.bundle()
		.pipe(source('index.js'))
		.pipe(rename('app.js'))
		.pipe(gulp.dest('assets/js'))

});


/**/
gulp.task('watch', function(){
	livereload.listen({basePath : 'assets'});
	gulp.watch('styles/style.styl', ['styles']);
	gulp.watch('src/index.js', ['scripts']);
});


//gulp.task('default', ['styles', 'complementCss', 'scripts', 'imagesGen', 'imagesSlider', 'imagesDepas', 'imagesTerre', 'imagesCond', 'watch']);
gulp.task('default', ['styles', 'complementCss', 'scripts', 'watch']);